#import tensorflow_hub as hub
from bertopic import BERTopic
from sentence_transformers import SentenceTransformer
from umap import UMAP
import hdbscan
import pickle


class TopicModel:

    def __init__(self):
        module_url = "encoder"
        self.embedding_model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
        #self.umap_model = UMAP(n_neighbors=15, n_components=5, metric='cosine', low_memory=False)
        #self.clusterer = hdbscan.HDBSCAN(min_cluster_size=10, metric='euclidean', prediction_data=True)

    def load(self):
        self.topic_model = BERTopic.load("topic_modeling/model/modeler", embedding_model=self.embedding_model)
        #topics = pickle.load(open("topics.pickle", "rb"))

    def train(self, docs):
        if docs.length == 0:
            return 0, 0
        hyper_params = {
            #'hdbscan_model': self.clusterer,
            #'umap_model': self.umap_model,
            'embedding_model': self.embedding_model
        }

        self.topic_model = BERTopic(**hyper_params)
        topics, probs = self.topic_model.fit_transform(docs)
        return topics, probs

    def update(self, docs, topics):
        self.topic_model.update_topics(docs, topics)

    def transform(self, docs):
        topics, probs = self.topic_model.transform(docs)
        return topics, probs

    def extract_topic_keywords(self, topic_id) -> list:
        topics = self.topic_model.get_topic(topic_id)
        return topics

    def find_topic(self, interest) -> list:
        topics, similarity = self.topic_model.find_topics(interest, top_n=2)
        return topics
