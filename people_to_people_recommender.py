from simillarity_engine import SimilarityEngine as se
from flask import Flask, request, jsonify
import database

user_ids, user_vectors, user_dobs, user_genders = database.getUsers()

similarity_engine = se(user_vectors)


def find_similar_friends(user_id):
    user_vector = database.get_user_vector(user_id)

    I, Ic = similarity_engine.search_similarity(user_vector, 5)

    cosine_similar_ids = Ic.tolist()
    similar_ids = I.tolist()

    c_index_users_ids = [user_ids[i]
                         for i in similar_ids[0] if user_ids[i] != user_id]
    #cosine
    cosine_index_user_ids = [user_ids[i]
                             for i in cosine_similar_ids[0] if user_ids[i] != user_id]

    recommended_friends = []
    for c_index, cosine_index in zip(c_index_users_ids, cosine_index_user_ids):
        if c_index not in recommended_friends:
            recommended_friends.append(c_index)
        if cosine_index not in recommended_friends:
            recommended_friends.append(cosine_index)

    return recommended_friends


app = Flask(__name__)


@app.route('/friends/recommend', methods=['GET'])
def friend_recommender():
    if request.method == 'GET':
        user_id = request.args.get('user')
        print(user_id)
        recommended_friends = find_similar_friends(int(user_id))

        data = {
            'recommended_friends': recommended_friends
        }

    return jsonify(data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4000, debug=True)
