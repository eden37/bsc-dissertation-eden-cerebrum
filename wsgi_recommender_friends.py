from people_to_people_recommender import app

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4000, debug=True)
